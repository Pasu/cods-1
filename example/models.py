from django.db import models
from django.contrib.auth.models import *
from datetime import timedelta

class Example(models.Model):
    contohId = models.AutoField(primary_key=True)
    tanggal = models.DateTimeField(default=timezone.now())
    tambahHari = models.IntegerField(default=0)
    tanggalHasilTambah = models.DateTimeField(editable=False,null=True)
    createdDate = models.DateTimeField(editable=False)
    lastModifiedDate = models.DateTimeField(null=True,editable=False)
    def save(self, *args, **kwargs):
        if not self.contohId:
            self.createdDate = timezone.now()
        else:
            self.lastModifiedDate = timezone.now()
        self.tanggalHasilTambah = self.tanggal + timedelta(days=3)
        return super(Example, self).save(*args, **kwargs)

    class Meta:
        db_table = 'example'

class Contoh(models.Model):
    contohid = models.AutoField(primary_key=True)
    deskripsi = models.CharField(max_length=128)
    class Meta:
        db_table = "contoh"
