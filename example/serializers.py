#rest framework
from rest_framework import *
from rest_framework.serializers import *
from rest_framework.authtoken.models import *

#django
from django.contrib.auth.models import *

from .models import *
class ExampleSerializer(ModelSerializer):
	class Meta:
		model = Example
		fields = "__all__"

class ContohSerializer(ModelSerializer):
	class Meta:
		model = Contoh
		fields = "__all__"
