#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *

#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *

from rest_framework.exceptions import PermissionDenied

class Examples(viewsets.ModelViewSet):
    serializer_class = ExampleSerializer
    queryset = Example.objects.all()

    """def list(self, request, *args, **kwargs):
        serializer = ExampleSerializer(self.get_queryset(), many=True)
        return Response({'result':serializer.data})"""

class Contoh(viewsets.ModelViewSet):
    serializer_class = ContohSerializer
    queryset = Contoh.objects.all()